# Silver Rainbow

Sixteen grayscale-ish color themes, eight dark and eight light.

A bit less busy, so maybe a bit less distracting.

## Dark themes

![Chi](img/chi.png)

![Darjeeling](img/darjeeling.png)

![Subgiant](img/subgiant.png)

![Goblin](img/goblin.png)

![Altostratus](img/altostratus.png)

![Silicon](img/silicon.png)

![Imperator](img/imperator.png)

![Mædi](img/maedi.png)

## Light themes

![Taiyō](img/taiyou.png)

![Iron Goddess](img/iron-goddess.png)

![Main Sequence](img/main-sequence.png)

![Sorcery](img/sorcery.png)

![Cirrus](img/cirrus.png)

![Oxygen](img/oxygen.png)

![Dauphin](img/dauphin.png)

![Diamond-Burned](img/diamond-burned.png)

## Info & contact

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Colors were chosen using [paletton](https://paletton.com).
The theme files contain hyperlinks to the specific palettes they use, if you're curious.

Each hue's light/dark pair follows a theme for naming:
reds are Japanese words, oranges are types of tea, yellows are astronomy terms, greens are fantasy terms, cyans are types of clouds, blues are elemental materials, purples are titles for rulers, and pinks are named for some of my genderlicious friends.
If ever I get around to high contrast themes, they would continue these respective trends.

I might have missed some colors, because I don't use every feature of VS Code.
Please [open an issue](https://gitlab.com/zephyrtronium/silver-rainbow/-/issues) if there are any colors that seem wrong, especially if they are wrong in multiple themes.
Or if you just have any feedback, really.
